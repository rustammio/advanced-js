class Employee {
    constructor(name, age, salary,) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(value) {
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    set salary(value) {
        this._salary = value;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, language) {
        super(name, age, salary);
        this._language = language;
    }
    get language() { return this._language}
    set language(language) { this._language = language}
    get salary() {
        return this._salary * 3;
    };

    set salary(value) {
        this._salary = value;
    }
}

const sam = new Programmer('Sam', 34, 5000, 'js')
const dave = new Programmer('Dave', 87, 13000, 'java')
const john = new Programmer('John', 57, 189000, 'go')

console.log(sam.age)
console.log(dave.name)
console.log(john.name)
console.log(sam.language)

